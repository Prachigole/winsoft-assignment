/* Q1: Merge two arrays by satisfying given constraints
Given two sorted arrays X[] and Y[] of size m and n each where m >= n and X[] has exactly n vacant cells,
 merge elements of Y[] in their correct position in array X[], i.e., merge (X, Y) by keeping the sorted order.

For example,

Input: X[] = { 0, 2, 0, 3, 0, 5, 6, 0, 0 }
Y[] = { 1, 8, 9, 10, 15 } The vacant cells in X[] is represented by 0 
Output: X[] = { 1, 2, 3, 5, 6, 8, 9, 10, 15 }
*/

import java.util.Arrays;
import java.util.Scanner;

class MergeArrays {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        System.out.print("Enter the size of X[] array: ");
        int m = scanner.nextInt();
        int[] X = new int[m];
        System.out.println("Enter the elements of X[] array (sorted with vacant cells represented by 0):");
        for (int i = 0; i < m; i++) {
            X[i] = scanner.nextInt();
        }


        System.out.print("Enter the size of Y[] array: ");
        int n = scanner.nextInt();
        int[] Y = new int[n];
        System.out.println("Enter the elements of Y[] array (sorted):");
        for (int i = 0; i < n; i++) {
            Y[i] = scanner.nextInt();
        }


        mergeArrays(X, Y);


        System.out.println("Merged array X[]: " + Arrays.toString(X));
    }

    public static void mergeArrays(int[] X, int[] Y) {
        int m = X.length;
        int n = Y.length;


        int i = m - 1;
        int j = m - 1;
        while (i >= 0) {
            if (X[i] != 0) {
                X[j] = X[i];
                j--;
            }
            i--;
        }


        i = j + 1;
        j = 0;
        int k = 0;
        while (i < m && j < n) {
            if (X[i] < Y[j]) {
                X[k] = X[i];
                i++;
            } else {
                X[k] = Y[j];
                j++;
            }
            k++;
        }


        while (j < n) {
            X[k] = Y[j];
            j++;
            k++;
        }
    }
}

