/* Q4:Write a Java Program to find the duplicate characters in a string.
*/
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

class DuplicateCharacters {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        
        System.out.print("Enter a string: ");
        String inputString = scanner.nextLine();

      
        Map<Character, Integer> charFrequency = findDuplicateCharacters(inputString);

     
        System.out.println("Duplicate characters in the string:");
        for (Map.Entry<Character, Integer> entry : charFrequency.entrySet()) {
            if (entry.getValue() > 1) {
                System.out.println(entry.getKey() + " - " + entry.getValue() + " occurrences");
            }
        }
    }

    public static Map<Character, Integer> findDuplicateCharacters(String inputString) {
        Map<Character, Integer> charFrequency = new HashMap<>();

    
        for (char c : inputString.toCharArray()) {
            charFrequency.put(c, charFrequency.getOrDefault(c, 0) + 1);
        }

        return charFrequency;
    }
}

