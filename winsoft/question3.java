/*       Q3:Write a Java Program to count the number of words in a string using HashMap.
*/

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

class WordCount {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        System.out.print("Enter a string: ");
        String inputString = scanner.nextLine();


        int wordCount = countWords(inputString);


        System.out.println("Total number of words: " + wordCount);
    }

    public static int countWords(String inputString) {

        String[] words = inputString.split("\\s+");


        Map<String, Integer> wordFreqMap = new HashMap<>();


        for (String word : words) {
            wordFreqMap.put(word, wordFreqMap.getOrDefault(word, 0) + 1);
        }


        return wordFreqMap.size();
    }
}


