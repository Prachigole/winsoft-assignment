/* Q2:Find maximum sum path involving elements of given arrays
Given two sorted arrays of integers, find a maximum sum path involving elements of both arrays whose sum is maximum. 
We can start from either array, but we can switch between arrays only through its common elements.

For example,

Input: X = { 3, 6, 7, 8, 10, 12, 15, 18, 100 }
Y = { 1, 2, 3, 5, 7, 9, 10, 11, 15, 16, 18, 25, 50 }  
The maximum sum path is: 1 —> 2 —> 3 —> 6 —> 7 —> 9 —> 10 —> 12 —> 15 —> 16 —> 18 —> 100 
The maximum sum is 199
*/

import java.util.Scanner;

class MaxSumPath {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        System.out.print("Enter the size of X[] array: ");
        int m = scanner.nextInt();
        int[] X = new int[m];
        System.out.println("Enter the elements of X[] array (sorted):");
        for (int i = 0; i < m; i++) {
            X[i] = scanner.nextInt();
        }


        System.out.print("Enter the size of Y[] array: ");
        int n = scanner.nextInt();
        int[] Y = new int[n];
        System.out.println("Enter the elements of Y[] array (sorted):");
        for (int i = 0; i < n; i++) {
            Y[i] = scanner.nextInt();
        }


        int maxSum = maxSumPath(X, Y);


        System.out.println("Maximum sum path: " + maxSum);
    }

    public static int maxSumPath(int[] X, int[] Y) {
        int m = X.length;
        int n = Y.length;
        int sumX = 0, sumY = 0, maxSum = 0;
        int i = 0, j = 0;

        while (i < m && j < n) {
            if (X[i] < Y[j]) {
                sumX += X[i];
                i++;
            } else if (X[i] > Y[j]) {
                sumY += Y[j];
                j++;
            } else {
                maxSum += Math.max(sumX, sumY) + X[i];
                sumX = 0;
                sumY = 0;
                i++;
                j++;
            }
        }

        while (i < m) {
            sumX += X[i];
            i++;
        }

        while (j < n) {
            sumY += Y[j];
            j++;
        }

        maxSum += Math.max(sumX, sumY);

        return maxSum;
    }
}

